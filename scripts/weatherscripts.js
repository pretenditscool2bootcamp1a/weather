"use strict";

let cities = [{
  name: "Detroit",
  latitude: 42.3314,
  longitude: -83.0458,
  imgfile: "images/detroit1.jpg"
},
{
  name: "Boston",
  latitude: 42.3601,
  longitude: -71.0589,
  imgfile: "images/boston1.jpg"
},
{
  name: "Seattle",
  latitude: 47.6062,
  longitude: -122.3321,
  imgfile: "images/seattle1.jpg"
},
{
  name: "San Fancisco",
  latitude: 37.7749,
  longitude: -122.4194,
  imgfile: "images/sanfrancisco1.jpg"
}];

document.onload = init();

function init() {
  const ptrselect = document.getElementById("cityselector");
  setupselect(ptrselect);
  ptrselect.onchange = tblupdate;
}

function setupselect(select1) {
  console.log("in setupselect");
  let theOption = new Option("Select One", "");
  select1.appendChild(theOption);
  let unsortedcitynames = getunsortedcitynames();
  console.log(unsortedcitynames);
  let sortedcitynames = "";

  sortedcitynames = unsortedcitynames.sort();

  for (let i = 0; i < sortedcitynames.length; i++) {
    let theOption = new Option(sortedcitynames[i],sortedcitynames[i]);

    select1.appendChild(theOption);
  }
  return false;
}

function cleartable(tbl1) {
  const tbltitle = document.getElementById("forecasttitle");
  const imgdiv = document.getElementById("imagediv");
  imgdiv.innerHTML = "" ;
  tbltitle.innerHTML = "";
  tbl1.innerHTML = "";
}

function getunsortedcitynames(inp1) {
  let retempnames = "";
  for (let i = 0; i < cities.length; i++) {
    retempnames = retempnames + "," + cities[i].name;
    console.log(retempnames);
  }
  retempnames = retempnames.substring(1);
  return retempnames.split(",");
}

function tblupdate() {
  const select1 = document.getElementById("cityselector");

  const tbl1 = document.getElementById("citiesTblBody");
  cleartable(tbl1);

  if (select1.selectedIndex == 0) {

    alert("please select an actual city from this drop down");


  }
  else {
    setselectedmessage(select1);
    setselectedpicture(select1.selectedIndex - 1);
    fetchselectedcityweatherlink (cities[select1.selectedIndex - 1].latitude, cities[select1.selectedIndex - 1].longitude) ;
    // console.log("in tblupdate about to call filltable function");
    // filltable(select1.selectedIndex, tbl1);
  }
}

function setselectedpicture (inparrayindex) {
  console.log (inparrayindex) ;
  const tblpicdiv = document.getElementById("imagediv");
  let oimg = document.createElement('img') ;
  oimg.setAttribute('src', cities[inparrayindex].imgfile);
  oimg.setAttribute('alt', "city picture") ;
  tblpicdiv.appendChild(oimg) ;
  
}

function fetchselectedcityweatherlink (latitude,longitude) {
  let stationlookupurl = `https://api.weather.gov/points/${latitude},${longitude}` ;
  console.log (stationlookupurl) ;
  fetch(stationlookupurl)
  .then((response) => response.json())
  .then((data) => {
    let weatherurl = data.properties.forecast;
    console.log (weatherurl) ;
    getweather(weatherurl) ;
  });
}

function getweather (inpweatherurl) {
  fetch(inpweatherurl)
  .then((response) => response.json())
  .then((data) => {
    let forecastarray = data.properties.periods;
    displayweather(forecastarray) ;
  });
}

function displayweather (inpforecastarray) {
  const ptrtblbody = document.getElementById("citiesTblBody") ;
  for (let i = 0 ; i < inpforecastarray.length; i++) {
    buildtablerow(ptrtblbody, "Time", inpforecastarray[i].name, false, "");
    buildtablerow(ptrtblbody, "Temperature", inpforecastarray[i].temperature + " " + inpforecastarray[i].temperatureUnit, false, "");
    buildtablerow(ptrtblbody, "Winds", inpforecastarray[i].windDirection + " " + inpforecastarray[i].windSpeed , false, "");
    buildtablerow(ptrtblbody, "ShortForecast", inpforecastarray[i].shortForecast, false, "");
  }
}

function buildtablerow(tbl1, fieldname, fieldvalue, clickable1, projs1) {
  // console.log("in buildtablerow ");
  let trObject = tbl1.insertRow(-1);
  trObject.scope = "col";
  let cell1 = trObject.insertCell(0);
  cell1.innerHTML = "<b>" + fieldname + "</b>";
  let cell2 = trObject.insertCell(1);
  cell2.innerHTML = fieldvalue;
  if (clickable1) {
    cell2.style = "background-color: coral" ;
    cell2.onclick = function () { showprojects(tbl1 , projs1) ; cell2.style = "background-color: white" ; ; cell2.onclick = "" ;}
  }
}

function setselectedmessage(inpptr1) {
  const tbltitle = document.getElementById("forecasttitle");
  tbltitle.innerHTML = "Forecast for : " + inpptr1.value;
}
function filltable(category1, tbl1) {
  const keys = Object.getOwnPropertyNames(cities[category1]);

  for (const [key, value] of Object.entries(cities[category1])) {
    if (`${key}` == "projectsAssignedTo") {
      let numofprojs = cities[category1].projectsAssignedTo.length;
      buildtablerow(tbl1, "Number of assigned projects", numofprojs, true, cities[category1].projectsAssignedTo);
    }
    else {
      buildtablerow(tbl1, `${key}`, `${value}`, false, "");
    }
  }
}

function buildtablerow(tbl1, fieldname, fieldvalue, clickable1, projs1) {
  // console.log("in buildtablerow ");
  let trObject = tbl1.insertRow(-1);
  trObject.scope = "col";
  let cell1 = trObject.insertCell(0);
  cell1.innerHTML = "<b>" + fieldname + "</b>";
  let cell2 = trObject.insertCell(1);
  cell2.innerHTML = fieldvalue;
  if (clickable1) {
    cell2.style = "background-color: coral" ;
    cell2.onclick = function () { showprojects(tbl1 , projs1) ; cell2.style = "background-color: white" ; ; cell2.onclick = "" ;}
  }
}

function showprojects(tbl1, inpprojs) {
  console.log(inpprojs);
  for (let i = 0; i < inpprojs.length; i++) {
    for (const [key, value] of Object.entries(inpprojs[i])){
      // console.log ("key " + `${key}` + " value "  + `${value}`) ;
      buildtablerow(tbl1, `${key}`, `${value}`, false, "");
    }
  }
} 